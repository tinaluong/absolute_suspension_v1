Rails.application.routes.draw do
  get 'customers/search'
  get 'vehicles/search'
  get 'pages/help'
  get 'reports/employee_hour_weekly'
  get 'reports/employee_hour_monthly'
  get 'reports/service_revenue_weekly'
  get 'reports/service_revenue_monthly'

  resources :reports
  resources :transmissions
  resources :models
  resources :makes
  resources :ticketstatuses
  resources :servicepositions
  resources :serviceactions
  resources :servicelines
  resources :employees
  resources :services
  resources :categories
  resources :tickets do
    resources :servicelines
  end
  resources :vehicles do
    resources :tickets
  end
  resources :customers do
    resources :vehicles do
      resources :tickets
    end
  end
  get 'tickets/index'
  get 'tickets/update_services', as: 'update_services'
  get 'tickets/show'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#home'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
