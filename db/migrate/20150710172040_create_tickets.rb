class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :note
      t.string :ticketstatus
      t.decimal :total
      t.integer :vehicle_id

      t.timestamps null: false
    end
  end
end
