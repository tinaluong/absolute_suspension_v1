class AddMakeKeyAndModelKeyAndTransmissionKeyToVehicle < ActiveRecord::Migration
  def change
    add_column :vehicles, :make_id, :integer
    add_column :vehicles, :model_id, :integer
    add_column :vehicles, :transmission_id, :integer
  end
end
