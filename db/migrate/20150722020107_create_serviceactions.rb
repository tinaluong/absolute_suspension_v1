class CreateServiceactions < ActiveRecord::Migration
  def change
    create_table :serviceactions do |t|
      t.string :action

      t.timestamps null: false
    end
  end
end
