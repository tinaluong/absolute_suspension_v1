class CreateTicketstatuses < ActiveRecord::Migration
  def change
    create_table :ticketstatuses do |t|
      t.string :status

      t.timestamps null: false
    end
  end
end
