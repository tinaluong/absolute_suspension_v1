class CreateServicelines < ActiveRecord::Migration
  def change
    create_table :servicelines do |t|
      t.string :action
      t.string :position
      t.decimal :hour
      t.decimal :rate
      t.string :part
      t.integer :quantity
      t.decimal :part_price
      t.decimal :subtotal
      t.integer :ticket_id
      t.integer :category_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
