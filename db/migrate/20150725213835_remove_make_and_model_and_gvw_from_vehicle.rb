class RemoveMakeAndModelAndGvwFromVehicle < ActiveRecord::Migration
  def change
    remove_column :vehicles, :make, :string
    remove_column :vehicles, :model, :string
    remove_column :vehicles, :gvw, :decimal
  end
end
