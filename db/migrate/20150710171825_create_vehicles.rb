class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :year
      t.string :make
      t.string :model
      t.decimal :gvw
      t.string :license
      t.decimal :mile
      t.string :engine
      t.string :vin
      t.integer :customer_id

      t.timestamps null: false
    end
  end
end
