class AddServiceactionKeyAndServicepositionKeyToServiceline < ActiveRecord::Migration
  def change
    add_column :servicelines, :serviceaction_id, :integer
    add_column :servicelines, :serviceposition_id, :integer
  end
end
