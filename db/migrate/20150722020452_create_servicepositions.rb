class CreateServicepositions < ActiveRecord::Migration
  def change
    create_table :servicepositions do |t|
      t.string :position

      t.timestamps null: false
    end
  end
end
