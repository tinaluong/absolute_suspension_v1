class RemoveActionAndPositionFromServiceline < ActiveRecord::Migration
  def change
    remove_column :servicelines, :action, :string
    remove_column :servicelines, :position, :string
  end
end
