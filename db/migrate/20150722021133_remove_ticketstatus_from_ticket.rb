class RemoveTicketstatusFromTicket < ActiveRecord::Migration
  def change
    remove_column :tickets, :ticketstatus, :string
  end
end
