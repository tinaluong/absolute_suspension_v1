# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  services = $('#serviceline_id_service_id').html()
  $('#serviceline_id_category_id').change ->
    category = $('#serviceline_id_category_id :selected').text()
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(services).filter("optgroup[label='#{escaped_category}']").html()
    if options
      $('#serviceline_id_service_id').html(options)
      $('#serviceline_id_service_id').parent().show()
    else
      $('#serviceline_id_service_id').empty()
      $('#serviceline_id_service_id').parent().hide()
  $('serviceline_id_category_id').trigger 'change'

$(document).on "click", "form .remove_fields", (event) ->
  $(this).prev("input[type=hidden]").val("1")
  $(this).closest("fieldset").hide()
  event.preventDefault()
$(document).on "click", "form .add_fields", (event) ->
  time = new Date().getTime()
  regexp = new RegExp($(this).data("id"), "g")
  $(this).before($(this).data("fields").replace(regexp, time ))
  event.preventDefault()

updateSubtotal = ->
  subotal = 0;
  hour = $('#ticket_servicelines_attributes_0_hour').val()
  rate = $('#ticket_servicelines_attributes_0_rate').val()
  quantity = $('#ticket_servicelines_attributes_0_quantity').val()
  price = $('#ticket_servicelines_attributes_0_part_price').val()
  subtotal = (hour * rate) + (quantity * price)
  $('#ticket_servicelines_attributes_0_subtotal').val(subtotal).toFixed(2)
$ ->
  $('#ticket_servicelines_attributes_0_hour,#ticket_servicelines_attributes_0_rate, #ticket_servicelines_attributes_0_quantity,
    #ticket_servicelines_attributes_0_part_price').bind 'keyup mouseup mousewheel', ->
    updateSubtotal()

