# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
jQuery ->
  loaded_model = $('#serviceline_service_id :selected').text()
  $('#serviceline_service_id').parent().hide()
  services = $('#serviceline_service_id').html()
  if loaded_model.length != 0
    category = $('#serviceline_category_id :selected').text()
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\][ ][()=>|\/@])/g, '\\$1')
    options = $(services).filter("optgroup[label='#{escaped_category}']").html()
    $('#serviceline_service_id').html(options)
    $('#serviceline_service_id').parent().show()
  $('#serviceline_category_id').change ->
    category = $('#serviceline_category_id :selected').text()
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\][ ][()=>|\/@])/g, '\\$1')
    options = $(services).filter("optgroup[label='#{escaped_category}']").html()
    if options
      $('#serviceline_service_id').html(options)
      $('#serviceline_service_id').parent().show()
    else
      $('#serviceline_service_id').empty()
      $('#serviceline_service_id').parent().hide()
  $('serviceline_category_id').trigger 'change'

updateSubtotal = ->
  subtotal = 0;
  hour = $('#serviceline_hour').val()
  rate = $('#serviceline_rate').val()
  quantity = $('#serviceline_quantity').val()
  price = $('#serviceline_part_price').val()
  subtotal = (hour * rate) + (quantity * price)
  $('#serviceline_subtotal').val(subtotal).toFixed(2)
$ ->
  $('#serviceline_hour,#serviceline_rate, #serviceline_quantity,
    #serviceline_part_price').bind 'keyup mouseup mousewheel', ->
    updateSubtotal()

goBack = ->
  window.history.back()
  return

ready = ->
  $("#backButton").click ->
    goBack()
    return
  return

$(document).ready(ready)
$(document).on('page:load', ready)
