# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  loaded_model = $('#vehicle_model_id :selected').text()
  $('#vehicle_model_id').parent().hide()
  models = $('#vehicle_model_id').html()
  if loaded_model.length != 0
    make = $('#vehicle_make_id :selected').text()
    escaped_make = make.replace(/([ #;&,.+*~\':"!^$[\][ ][()=>|\/@])/g, '\\$1')
    options = $(models).filter("optgroup[label='#{escaped_make}']").html()
    $('#vehicle_model_id').html(options)
    $('#vehicle_model_id').parent().show()
  $('#vehicle_make_id').change ->
    make = $('#vehicle_make_id :selected').text()
    escaped_make = make.replace(/([ #;&,.+*~\':"!^$[\][ ][()=>|\/@])/g, '\\$1')
    options = $(models).filter("optgroup[label='#{escaped_make}']").html()
    if options
      $('#vehicle_model_id').html(options)
      $('#vehicle_model_id').parent().show()
    else
      $('#vehicle_model_id').empty()
      $('#vehicle_model_id').parent().hide()
  $('vehicle_make_id').trigger 'change'
