class VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]

  # GET /vehicles
  # GET /vehicles.json
  def index
    @vehicles = Vehicle.order("created_at DESC").paginate(:page => params[:page], :per_page => 30)
    respond_to do |format|
      format.html  #index.html.erb
      format.json { render json: @vehicles }
    @makes = Make.all
    @models = Model.where("make_id = ?", Make.first.id)
    if params[:simplesearch]
      @vehicles = Vehicle.simplesearch(params[:simplesearch]).order("last_name ASC, first_name ASC").paginate(:page => params[:page], :per_page => 30)
      if @vehicles.blank?
        flash.now[:alert] = "Your search does not match any record. Please try again or Create A New Vehicle."
      end
    else
      @vehicles = Vehicle.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 30)
    end
    end
  end

  def search
    @vehicles = Vehicle.search(params[:search], params[:condition])
    if @vehicles.blank?
      flash.now[:alert] = "Your search does not match any record. Please try again or Create A New Ticket."
    end
  end
  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
  end

  def update_models
    @models = Model.where("make_id = ?", params[:make_id])
    respond_to do |format|
      format.js
    end
  end

  # GET /vehicles/new
  def new
    @vehicle = Vehicle.new
  end

  # GET /vehicles/1/edit
  def edit
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle = Vehicle.new(vehicle_params)

    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to @vehicle, notice: 'Vehicle was successfully created.' }
        format.json { render :show, status: :created, location: @vehicle }
      else
        format.html { render :new }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update
    respond_to do |format|
      if @vehicle.update(vehicle_params)
        format.html { redirect_to @vehicle, notice: 'Vehicle was successfully updated.' }
        format.json { render :show, status: :ok, location: @vehicle }
      else
        format.html { render :edit }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.html { redirect_to vehicles_url, notice: 'Vehicle was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.require(:vehicle).permit(:year, :make, :model, :gvw, :license, :mile, :engine, :vin, :customer_id,
      :make_id, :model_id, :transmission_id)
    end
end
