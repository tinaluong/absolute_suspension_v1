class PagesController < ApplicationController
  def home
    @tickets = Ticket.order("created_at DESC")
    @pending_tickets = Ticket.order("created_at DESC").where(:ticketstatus_id => 1)
    @complete_tickets = Ticket.order("created_at DESC").where(:ticketstatus_id => 2)
    respond_to do |format|
      format.html
      format.pdf
    end
  end
  def help

  end
end
