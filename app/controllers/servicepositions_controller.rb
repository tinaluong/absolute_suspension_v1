class ServicepositionsController < ApplicationController
  before_action :set_serviceposition, only: [:show, :edit, :update, :destroy]

  # GET /servicepositions
  # GET /servicepositions.json
  def index
    @servicepositions = Serviceposition.all
  end

  # GET /servicepositions/1
  # GET /servicepositions/1.json
  def show
  end

  # GET /servicepositions/new
  def new
    @serviceposition = Serviceposition.new
  end

  # GET /servicepositions/1/edit
  def edit
  end

  # POST /servicepositions
  # POST /servicepositions.json
  def create
    @serviceposition = Serviceposition.new(serviceposition_params)

    respond_to do |format|
      if @serviceposition.save
        format.html { redirect_to @serviceposition, notice: 'Serviceposition was successfully created.' }
        format.json { render :show, status: :created, location: @serviceposition }
      else
        format.html { render :new }
        format.json { render json: @serviceposition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /servicepositions/1
  # PATCH/PUT /servicepositions/1.json
  def update
    respond_to do |format|
      if @serviceposition.update(serviceposition_params)
        format.html { redirect_to @serviceposition, notice: 'Serviceposition was successfully updated.' }
        format.json { render :show, status: :ok, location: @serviceposition }
      else
        format.html { render :edit }
        format.json { render json: @serviceposition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servicepositions/1
  # DELETE /servicepositions/1.json
  def destroy
    @serviceposition.destroy
    respond_to do |format|
      format.html { redirect_to servicepositions_url, notice: 'Serviceposition was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_serviceposition
      @serviceposition = Serviceposition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def serviceposition_params
      params.require(:serviceposition).permit(:position)
    end
end
