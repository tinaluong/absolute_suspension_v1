class ServiceactionsController < ApplicationController
  before_action :set_serviceaction, only: [:show, :edit, :update, :destroy]

  # GET /serviceactions
  # GET /serviceactions.json
  def index
    @serviceactions = Serviceaction.all
  end

  # GET /serviceactions/1
  # GET /serviceactions/1.json
  def show
  end

  # GET /serviceactions/new
  def new
    @serviceaction = Serviceaction.new
  end

  # GET /serviceactions/1/edit
  def edit
  end

  # POST /serviceactions
  # POST /serviceactions.json
  def create
    @serviceaction = Serviceaction.new(serviceaction_params)

    respond_to do |format|
      if @serviceaction.save
        format.html { redirect_to @serviceaction, notice: 'Serviceaction was successfully created.' }
        format.json { render :show, status: :created, location: @serviceaction }
      else
        format.html { render :new }
        format.json { render json: @serviceaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /serviceactions/1
  # PATCH/PUT /serviceactions/1.json
  def update
    respond_to do |format|
      if @serviceaction.update(serviceaction_params)
        format.html { redirect_to @serviceaction, notice: 'Serviceaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @serviceaction }
      else
        format.html { render :edit }
        format.json { render json: @serviceaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /serviceactions/1
  # DELETE /serviceactions/1.json
  def destroy
    @serviceaction.destroy
    respond_to do |format|
      format.html { redirect_to serviceactions_url, notice: 'Serviceaction was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_serviceaction
      @serviceaction = Serviceaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def serviceaction_params
      params.require(:serviceaction).permit(:action)
    end
end
