class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.order("created_at DESC").paginate(:page => params[:page], :per_page => 15)
    respond_to do |format|
      format.html  #index.html.erb
      format.json { render json: @customers }

    if params[:simplesearch]
      @customers = Customer.simplesearch(params[:simplesearch]).order("last_name ASC, first_name ASC").paginate(page: params[:page], per_page: 15)
      if @customers.blank?
        flash.now[:alert] = "Your search does not match any record. Please try again or Create A New Customer."
      end
    else
      @customers = Customer.all.order("created_at DESC").paginate(page: params[:page], per_page: 15)
    end
    end
  end

  def search
    @customers = Customer.search(params[:search], params[:condition])
    if @customers.blank?
      flash.now[:alert] = "Your search does not match any record. Please try again or Create A New Ticket."
    end
  end
  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Customer was successfully created.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:last_name, :first_name, :address, :city, :state, :zip, :phone, :email, :additional_contact)
    end
end
