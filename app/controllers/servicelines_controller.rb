class ServicelinesController < ApplicationController
  before_action :set_serviceline, only: [:show, :edit, :update, :destroy]

  # GET /servicelines
  # GET /servicelines.json
  def index
    @servicelines = Serviceline.order("created_at DESC")
    @brake = Serviceline.order("created_at DESC").where(:category_id => 1)
    @suspension = Serviceline.order("created_at DESC").where(:category_id => 2)
    @outside = Serviceline.order("created_at DESC").where(:category_id => 3)
    respond_to do |format|
      format.html  #index.html.erb
      format.json { render json: @servicelines }
    end
    @categories = Category.all
    @services = Service.where("category_id = ?", Category.first.id)
  end

  def show
  end

  def update_services
    @services = Service.where("category_id = ?", params[:category_id])
    respond_to do |format|
      format.js
    end
  end

  # GET /servicelines/new
  def new
    @serviceline = Serviceline.new
  end

  # GET /servicelines/1/edit
  def edit
  end

  # POST /servicelines
  # POST /servicelines.json
  def create
    @serviceline = Serviceline.new(serviceline_params)

    respond_to do |format|
      if @serviceline.save
        format.html { redirect_to @serviceline, notice: 'Serviceline was successfully created.' }
        format.json { render :show, status: :created, location: @serviceline }
      else
        format.html { render :new }
        format.json { render json: @serviceline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /servicelines/1
  # PATCH/PUT /servicelines/1.json
  def update
    respond_to do |format|
      if @serviceline.update(serviceline_params)
        format.html { redirect_to @serviceline, notice: 'Serviceline was successfully updated.' }
        format.json { render :show, status: :ok, location: @serviceline }
      else
        format.html { render :edit }
        format.json { render json: @serviceline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servicelines/1
  # DELETE /servicelines/1.json
  def destroy
    @serviceline.destroy
    respond_to do |format|
      format.html { redirect_to servicelines_url, notice: 'Serviceline was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_serviceline
      @serviceline = Serviceline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def serviceline_params
      params.require(:serviceline).permit(:action, :position, :hour, :rate, :part, :quantity, :service_id,
                                          :part_price, :subtotal, :ticket_id, :category_id, :employee_id, :serviceaction_id, :serviceposition_id)
    end
end
