class ReportsController < ApplicationController
  def employee_hour_weekly
    @weeklyhours1 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Bates").id, created_at: [1.week.ago..Time.now])
    @weeklyhours2 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Fierro").id, created_at: [1.week.ago..Time.now])
    @weeklyhours3 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Koslosky").id, created_at: [1.week.ago..Time.now])
    respond_to do |format|
      format.html
      format.pdf
    end
  end
  def employee_hour_monthly
    @monthlyhours1 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Bates").id, created_at: [1.month.ago..Time.now])
    @monthlyhours2 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Fierro").id, created_at: [1.month.ago..Time.now])
    @monthlyhours3 = Serviceline.order("created_at DESC").
        where(:employee_id => Employee.find_by(last_name: "Koslosky").id, created_at: [1.month.ago..Time.now])
    respond_to do |format|
      format.html
      format.pdf
    end
  end
  def service_revenue_weekly
    @totalweek = Serviceline.where(created_at: [1.week.ago..Time.now])
    @weeklyrevenue1 = Serviceline.where(:category_id => Category.find_by(category_name: "Brakes").id,
                                        created_at: [1.week.ago..Time.now])
    @weeklyrevenue2 = Serviceline.where(:category_id => Category.find_by(category_name: "Suspension").id,
                                        created_at: [1.week.ago..Time.now])
    @weeklyrevenue3 = Serviceline.where(:category_id => Category.find_by(category_name: "Outside Services").id,
                                        created_at: [1.week.ago..Time.now])
    respond_to do |format|
      format.html
      format.pdf
    end
  end
  def service_revenue_monthly
    @totalmonth = Serviceline.where(created_at: [1.month.ago..Time.now])
    @monthlyrevenue1 = Serviceline.where(:category_id => Category.find_by(category_name: "Brakes").id,
                                        created_at: [1.month.ago..Time.now])
    @monthlyrevenue2 = Serviceline.where(:category_id => Category.find_by(category_name: "Suspension").id,
                                        created_at: [1.month.ago..Time.now])
    @monthlyrevenue3 = Serviceline.where(:category_id => Category.find_by(category_name: "Outside Services").id,
                                        created_at: [1.month.ago..Time.now])
    respond_to do |format|
      format.html
      format.pdf
    end
  end
end
