class TicketstatusesController < ApplicationController
  before_action :set_ticketstatus, only: [:show, :edit, :update, :destroy]

  # GET /ticketstatuses
  # GET /ticketstatuses.json
  def index
    @ticketstatuses = Ticketstatus.all
  end

  # GET /ticketstatuses/1
  # GET /ticketstatuses/1.json
  def show
  end

  # GET /ticketstatuses/new
  def new
    @ticketstatus = Ticketstatus.new
  end

  # GET /ticketstatuses/1/edit
  def edit
  end

  # POST /ticketstatuses
  # POST /ticketstatuses.json
  def create
    @ticketstatus = Ticketstatus.new(ticketstatus_params)

    respond_to do |format|
      if @ticketstatus.save
        format.html { redirect_to @ticketstatus, notice: 'Ticketstatus was successfully created.' }
        format.json { render :show, status: :created, location: @ticketstatus }
      else
        format.html { render :new }
        format.json { render json: @ticketstatus.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ticketstatuses/1
  # PATCH/PUT /ticketstatuses/1.json
  def update
    respond_to do |format|
      if @ticketstatus.update(ticketstatus_params)
        format.html { redirect_to @ticketstatus, notice: 'Ticketstatus was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticketstatus }
      else
        format.html { render :edit }
        format.json { render json: @ticketstatus.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticketstatuses/1
  # DELETE /ticketstatuses/1.json
  def destroy
    @ticketstatus.destroy
    respond_to do |format|
      format.html { redirect_to ticketstatuses_url, notice: 'Ticketstatus was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticketstatus
      @ticketstatus = Ticketstatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticketstatus_params
      params.require(:ticketstatus).permit(:status)
    end
end
