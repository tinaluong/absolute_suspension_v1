class Model < ActiveRecord::Base
  belongs_to :make
  has_many :vehicles, :dependent => :destroy
  validates_presence_of :model
end
