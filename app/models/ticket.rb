class Ticket < ActiveRecord::Base
  has_many :servicelines, :dependent => :destroy
  belongs_to :vehicle
  belongs_to :ticketstatus

  def self.simplesearch(simplesearch)
    if simplesearch
      return includes(:vehicle).
          where("make_id LIKE ? or model_id LIKE ?", "%#{simplesearch}%", "%#{simplesearch}%").
          references(:vehicle)
    else
      scoped
    end
  end
  def calculate_total!
    self.update_column(:total, self.servicelines.sum(:subtotal))
  end

  def ticket_info
    "##{vehicle.id} - #{vehicle.make.make}, #{vehicle.model.model} - #{vehicle.customer.full_name}"
  end
end
