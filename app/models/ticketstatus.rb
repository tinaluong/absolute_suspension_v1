class Ticketstatus < ActiveRecord::Base
  has_many :tickets, :dependent => :destroy
  validates_presence_of :status
end
