class Serviceaction < ActiveRecord::Base
  has_many :servicelines, :dependent => :destroy
  validates_presence_of :action
end
