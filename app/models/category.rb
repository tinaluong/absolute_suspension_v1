class Category < ActiveRecord::Base
  has_many :services, :dependent => :destroy
  has_many :servicelines, :dependent => :destroy
  validates_presence_of :category_name
end
