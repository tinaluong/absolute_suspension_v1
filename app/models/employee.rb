class Employee < ActiveRecord::Base
  has_many :servicelines, :dependent => :destroy
  validates_presence_of :last_name
  validates_presence_of :first_name
  validates_presence_of :phone
  validates :phone, length: { minimum: 10, maximum: 12 }

  def full_name
    "#{last_name}, #{first_name}"
  end
end
