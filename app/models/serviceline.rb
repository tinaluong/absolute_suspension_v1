class Serviceline < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :category
  belongs_to :employee
  belongs_to :service
  belongs_to :serviceaction
  belongs_to :serviceposition
  after_save :calculate_total

  def calculate_total
    self.ticket.calculate_total!
  end
  #before_save :calculate_subtotal

  # def calculate_subtotal
  #   self.subtotal += ((self.hour * self.rate)+(self.quantity * self.part_price)).round(2)
  # end
end
