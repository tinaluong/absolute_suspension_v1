class Service < ActiveRecord::Base
  belongs_to :category
  has_many :servicelines, :dependent => :destroy
  validates_presence_of :service_name
end
