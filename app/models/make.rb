class Make < ActiveRecord::Base
  has_many :models, :dependent => :destroy
  has_many :vehicles, :dependent => :destroy
  validates_presence_of :make
end
