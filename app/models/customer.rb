class Customer < ActiveRecord::Base
  has_many :vehicles, :dependent => :destroy

  validates_presence_of :last_name
  validates_presence_of :first_name
  validates_presence_of :phone
  validates :state, length: {maximum:2}, :allow_blank => true
  validates :zip, length: {maximum:10}, :allow_blank => true
  validates :phone, length: { minimum: 10, maximum: 12 }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, }, :allow_blank => true

  def full_name
    "#{last_name}, #{first_name}"
  end

  def self.simplesearch(simplesearch)
    if simplesearch
      where('last_name LIKE ? OR first_name LIKE ? OR phone LIKE ?', "%#{simplesearch}%", "%#{simplesearch}%", "%#{simplesearch}%")
    else
      scoped
    end
  end

  def self.search(search, condition)
    if condition
      self.where("#{condition[0]} LIKE ?", '%'+search+'%')
    else
      []
    end
  end

end
