class Vehicle < ActiveRecord::Base
  belongs_to :customer
  belongs_to :make
  belongs_to :model
  belongs_to :transmission
  has_many :tickets, :dependent => :destroy
  validates_presence_of :customer
  validates_presence_of :make
  validates_presence_of :model
  validates_presence_of :transmission
  validates_presence_of :license
  validates_presence_of :vin
  validates :vin, length: {maximum:17}
  before_save :uppercase

  def uppercase
    self.license.upcase!
    self.vin.upcase!
  end
  def customer_vehicle
    "#{customer.full_name} - #{year}, #{make.make}, #{model.model}, #{license}"
  end
  def vehicle_info
    "#{year}, #{make.make} - #{model.model}, #{license}"
  end
  def yearmakemodel
    "#{year}, #{make.make} - #{model.model}"
  end

  def self.simplesearch(simplesearch)
    if simplesearch
      return includes(:make, :model, :customer).
          where("make LIKE ? OR model LIKE ? OR first_name LIKE ? or last_name LIKE ?", "%#{simplesearch}%", "%#{simplesearch}%", "%#{simplesearch}%", "%#{simplesearch}%").
          references(:make, :model, :customer)
    else
      scoped
    end
  end

  def self.search(search, condition)
    if condition
      self.where("#{condition[0]} LIKE ?", '%'+search+'%')
    else
      []
    end
  end
  # def self.search(search, condition)
  #   if condition
  #     return includes(:make, :model, :customer).
  #     where("#{condition[0]} LIKE ? OR make LIKE ? OR model LIKE ? OR first_name LIKE ? OR last_name LIKE ?",'%'+search+'%', "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%").
  #     references(:make, :model, :customer)
  #   else
  #     []
  #   end
  # end

end
