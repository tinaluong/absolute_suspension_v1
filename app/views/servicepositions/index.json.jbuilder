json.array!(@servicepositions) do |serviceposition|
  json.extract! serviceposition, :id, :position
  json.url serviceposition_url(serviceposition, format: :json)
end
