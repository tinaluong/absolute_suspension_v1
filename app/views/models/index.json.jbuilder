json.array!(@models) do |model|
  json.extract! model, :id, :model, :make_id
  json.url model_url(model, format: :json)
end
