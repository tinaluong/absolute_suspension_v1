json.array!(@customers) do |customer|
  json.extract! customer, :id, :last_name, :first_name, :address, :city, :state, :zip, :phone, :email, :additional_contact
  json.url customer_url(customer, format: :json)
end
