json.extract! @customer, :id, :last_name, :first_name, :address, :city, :state, :zip, :phone, :email, :additional_contact, :created_at, :updated_at
