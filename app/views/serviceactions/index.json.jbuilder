json.array!(@serviceactions) do |serviceaction|
  json.extract! serviceaction, :id, :action
  json.url serviceaction_url(serviceaction, format: :json)
end
