pdf.text "Absolute Suspension", :size =>13, :style => :bold, :align => :right
pdf.text "202 Foster Street", :size =>10, :style => :bold, :align => :right
pdf.text "Tomball, TX 77375", :size =>10, :style => :bold, :align => :right
pdf.text "(281) 351-7084", :size =>10, :style => :bold, :align => :right
pdf.move_down(30)
pdf.text "Report - Total Services Revenue In Last 1 Week", :align => :center, :size => 20, :style => :bold
pdf.text "(From #{1.week.ago.strftime("%D")} to #{Time.now.strftime("%D")})", :align => :center, :size =>15, :style => :bold, :color => "3498db"
pdf.move_down(20)
pdf.stroke_horizontal_rule
pdf.move_down(20)
weekly_revenue = [["Service Type", "Total Services", "Revenue"]]
weekly_revenue += [
    ["Brakes", "#{@weeklyrevenue1.count}", "#{number_to_currency(@weeklyrevenue1.sum(:subtotal))}"],
    ["Suspension", "#{@weeklyrevenue2.count}", "#{number_to_currency(@weeklyrevenue2.sum(:subtotal))}"],
    ["Outside Services", "#{@weeklyrevenue3.count}", "#{number_to_currency(@weeklyrevenue3.sum(:subtotal))}"],
    ["Weekly Total", "#{@totalweek.count}", "#{number_to_currency(@totalweek.sum(:subtotal))}"]
]

pdf.table(weekly_revenue, :header => true, :row_colors => ["d9edf7","d9edf7"], :width => 400, :position => :center,
:cell_style => { size: 10 }) do |table|
    table.row(0).font_style = :bold
    table.row(0).background_color = "FFFFFF"
    table.row(4).font_style = :bold
    table.row(4).background_color = "FFF1E5"
end