json.array!(@services) do |service|
  json.extract! service, :id, :service_name, :category_id
  json.url service_url(service, format: :json)
end
