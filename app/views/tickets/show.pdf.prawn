pdf.text "Absolute Suspension", :size =>13, :style => :bold, :align => :right
pdf.text "202 Foster Street", :size =>10, :style => :bold, :align => :right
pdf.text "Tomball, TX 77375", :size =>10, :style => :bold, :align => :right
pdf.text "(281) 351-7084", :size =>10, :style => :bold, :align => :right
pdf.move_down(20)
pdf.text "Repair Ticket ##{@ticket.id}", :align => :center, :size => 20, :style => :bold
pdf.text "#{Time.now.strftime("%D")} - Status: #{@ticket.ticketstatus.status}", :align => :center, :size => 15, :style => :bold
pdf.move_down(15)

pdf.stroke_horizontal_rule
pdf.move_down(15)
pdf.text "#{@ticket.vehicle.customer.full_name}", :size => 15, :style => :bold
pdf.text "Phone: #{number_to_phone(@ticket.vehicle.customer.phone, area_code: true)}", :size => 10
pdf.text "Vehicle: #{@ticket.vehicle.yearmakemodel}", :size => 10
pdf.text "License Plate: #{@ticket.vehicle.license}", :size => 10
pdf.text "Note: #{@ticket.note}", :size => 10
pdf.move_down(5)
pdf.text "Services", :size => 12, :style => :bold, :align => :center
pdf.move_down(5)
lines = [["Category", "Service", "Action", "Position", "Employee", "Hour", "Rate", "Part", "Quantity", "Price", "Subtotal"]]
lines += @ticket.servicelines.map do |line|
    [
        line.category.category_name,
        if line.service; line.service.service_name end,
        line.serviceaction.action,
        line.serviceposition.position,
        line.employee.full_name,
        line.hour,
        number_to_currency(line.rate),
        line.part,
        line.quantity,
        number_to_currency(line.part_price),
        number_to_currency(line.subtotal)
    ]
end
pdf.table(lines, :header => true, :row_colors => ["CCEBF7","ffffff"],
:cell_style => { size: 7.5 }) do |table|
    table.row(0).font_style = :bold
    table.row(0).background_color = "D0D0D0"
end
pdf.move_down(10)

if @ticket.ticketstatus_id == 1
    pdf.text "Estimated Subtotal: #{number_to_currency(@ticket.total)}", :size => 12, :style => :bold, :align => :right
else
    pdf.text "Total: #{number_to_currency(@ticket.total)}", :size => 12, :style => :bold, :align => :right
end;
pdf.move_down(10)
pdf.stroke_horizontal_rule

pdf.move_down(30)
pdf.text "___Vehicle may have other needs at a later date due to high mileage, age, and fatigue. Customer was advised and is aware and has declined at this time.", :size =>8
pdf.move_down(10)
pdf.text "*NOTICE FOR PAYMENTS BY CREDIT CARDS AND CHECKS (required by Chapter 70 Texas Property Code)", :size => 8, :style => :bold
pdf.text "I have previously given authorization to make repairs and test drive the vehicle. Check and Credit Cards payments: I am the person or an agent acting on behalf of the person who is obligated to pay for the repairs to the above mentioned motor vehicle subject to this statement. I understand that this vehicle is subject to repossession in accordance with session 9.609, Texas Business and Commerce Code, if a check, money order, or credit card transaction for payment for repairs of the vehicle is stopped, or is dishonored because of insufficient funds, no funds, or because the drawer or maker of the order has no account of the account on which it is drawn has been closed.", :size => 8
pdf.text "I hereby authorize the above work to be done along with the necessary materials. You and your employees may operate the vehicle for the purpose of testing, inspection, and/or delivery at my risk.", :size => 8
pdf.move_down(10)

pdf.text "Customer's Signature", :size => 8, :style => :bold
pdf.move_down(60)
pdf.text "x_________________________________"
