json.array!(@ticketstatuses) do |ticketstatus|
  json.extract! ticketstatus, :id, :status
  json.url ticketstatus_url(ticketstatus, format: :json)
end
