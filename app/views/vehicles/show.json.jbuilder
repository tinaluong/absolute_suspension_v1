json.extract! @vehicle, :id, :year, :make_id, :model_id, :gvw, :license, :mile, :engine, :transmission_id, :vin, :customer_id, :created_at, :updated_at
