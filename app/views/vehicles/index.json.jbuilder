json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :year, :make_id, :model_id, :gvw, :license, :mile, :engine, :transmission_id, :vin, :customer_id
  json.url vehicle_url(vehicle, format: :json)
end
