json.array!(@servicelines) do |serviceline|
  json.extract! serviceline, :id, :action_id, :position_id, :hour, :rate, :part, :quantity, :part_price, :subtotal, :ticket_id, :category_id, :employee_id
  json.url serviceline_url(serviceline, format: :json)
end
