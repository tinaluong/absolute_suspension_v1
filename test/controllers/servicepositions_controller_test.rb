require 'test_helper'

class ServicepositionsControllerTest < ActionController::TestCase
  setup do
    @serviceposition = servicepositions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:servicepositions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create serviceposition" do
    assert_difference('Serviceposition.count') do
      post :create, serviceposition: { position: @serviceposition.position }
    end

    assert_redirected_to serviceposition_path(assigns(:serviceposition))
  end

  test "should show serviceposition" do
    get :show, id: @serviceposition
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @serviceposition
    assert_response :success
  end

  test "should update serviceposition" do
    patch :update, id: @serviceposition, serviceposition: { position: @serviceposition.position }
    assert_redirected_to serviceposition_path(assigns(:serviceposition))
  end

  test "should destroy serviceposition" do
    assert_difference('Serviceposition.count', -1) do
      delete :destroy, id: @serviceposition
    end

    assert_redirected_to servicepositions_path
  end
end
