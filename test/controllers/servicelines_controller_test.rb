require 'test_helper'

class ServicelinesControllerTest < ActionController::TestCase
  setup do
    @serviceline = servicelines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:servicelines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create serviceline" do
    assert_difference('Serviceline.count') do
      post :create, serviceline: { action: @serviceline.action, category_id: @serviceline.category_id, employee_id: @serviceline.employee_id, hour: @serviceline.hour, part: @serviceline.part, part_price: @serviceline.part_price, position: @serviceline.position, quantity: @serviceline.quantity, rate: @serviceline.rate, subtotal: @serviceline.subtotal, ticket_id: @serviceline.ticket_id }
    end

    assert_redirected_to serviceline_path(assigns(:serviceline))
  end

  test "should show serviceline" do
    get :show, id: @serviceline
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @serviceline
    assert_response :success
  end

  test "should update serviceline" do
    patch :update, id: @serviceline, serviceline: { action: @serviceline.action, category_id: @serviceline.category_id, employee_id: @serviceline.employee_id, hour: @serviceline.hour, part: @serviceline.part, part_price: @serviceline.part_price, position: @serviceline.position, quantity: @serviceline.quantity, rate: @serviceline.rate, subtotal: @serviceline.subtotal, ticket_id: @serviceline.ticket_id }
    assert_redirected_to serviceline_path(assigns(:serviceline))
  end

  test "should destroy serviceline" do
    assert_difference('Serviceline.count', -1) do
      delete :destroy, id: @serviceline
    end

    assert_redirected_to servicelines_path
  end
end
