require 'test_helper'

class ServiceactionsControllerTest < ActionController::TestCase
  setup do
    @serviceaction = serviceactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:serviceactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create serviceaction" do
    assert_difference('Serviceaction.count') do
      post :create, serviceaction: { action: @serviceaction.action }
    end

    assert_redirected_to serviceaction_path(assigns(:serviceaction))
  end

  test "should show serviceaction" do
    get :show, id: @serviceaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @serviceaction
    assert_response :success
  end

  test "should update serviceaction" do
    patch :update, id: @serviceaction, serviceaction: { action: @serviceaction.action }
    assert_redirected_to serviceaction_path(assigns(:serviceaction))
  end

  test "should destroy serviceaction" do
    assert_difference('Serviceaction.count', -1) do
      delete :destroy, id: @serviceaction
    end

    assert_redirected_to serviceactions_path
  end
end
