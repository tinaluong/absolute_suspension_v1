require 'test_helper'

class TicketstatusesControllerTest < ActionController::TestCase
  setup do
    @ticketstatus = ticketstatuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ticketstatuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticketstatus" do
    assert_difference('Ticketstatus.count') do
      post :create, ticketstatus: { status: @ticketstatus.status }
    end

    assert_redirected_to ticketstatus_path(assigns(:ticketstatus))
  end

  test "should show ticketstatus" do
    get :show, id: @ticketstatus
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticketstatus
    assert_response :success
  end

  test "should update ticketstatus" do
    patch :update, id: @ticketstatus, ticketstatus: { status: @ticketstatus.status }
    assert_redirected_to ticketstatus_path(assigns(:ticketstatus))
  end

  test "should destroy ticketstatus" do
    assert_difference('Ticketstatus.count', -1) do
      delete :destroy, id: @ticketstatus
    end

    assert_redirected_to ticketstatuses_path
  end
end
